var CONFIG = {
    /*** Event display ***/
    /* Sections */
    "propertiesSectionClass": "properties-section",
    "productsSectionClass"  : "products-section",
    /* Id */
    "idElementClass"        : "id",
    /* Properties */
    "visitorAnchorTabIndex"  : 3,
    "productAnchorTabIndex"  : 3,
    "propertyClass"         : "property",
    "propertyNameClass"     : "name",
    "propertyValueClass"    : "value",
    "visitorPropertyClass"  : "visitor",
    "datePropertyClass"     : "date",
    "randomPropertyClass"   : "random",
    "visitorPropertyName"   : "Visitor:",
    "datePropertyName"      : "Date:",
    "randomPropertyName"    : "Random:",
    "visitorAnchorTitle"    : "Click show only events of this visitor",
    /* Products */
    "productsContainerClass"     : "products-container",
    "outerProductsContainerClass": "outer",
    "productsLabelClass"         : "products-label",
    "productsListContainerClass" : "products-list-container",
    "productsListClass"          : "products-list",
    "productClass"               : "product",
    "productRecommendedClass"    : "recommended",
    "productImageZoomedClass"    : "zoomed-image",
    "productHoverClass"          : "hover",
    "impressionsLabel"           : "Impressions:",
    "viewedProductsLabel"        : "Viewed products:",
    "addedProductsLabel"         : "Added products:",
    "purchasedProductsLabel"     : "Purchased products:",
    "cartLabel"                  : "Cart:",
    "productAnchorTitle"         : "Click to show only events mentioning this product",
    "collapsingPanelClass"       : "collapsing-panel",
    "collapsingPanelElementClass": "collapsing-element",
    "panelCollapsedClass"        : "collapsed",
    "panelExpandedClass"         : "expanded",
    "collapsingPanelDisabled"    : "disabled",
    "collapsingPanelButtonClass" : "collapsing-button",
    "collapsingPanelButtonText"  : "Expand",
    "collapsingPanelRefreshTime" : 100,

    /*** Page numbers ***/
    "initialPageNumber"      : 1,
    "pageNumbersDisplayed"   : 9,
    "pageNumberClass"       : "page-number",
    "currentPageNumberClass": "current",
    "firstPageNumberClass"  : "first",
    "lastPageNumberClass"   : "last",
    "numberInputMessageTime": 5000,

    /*** Other ***/
    "anchorClass"           : "anchor",

    /*** State URL ***/
    "stateStartMarker"      : "?",
    "statePropertySeparator": "&",
    "filterVisitorName"     : "visitor",
    "filterProductName"     : "product",

    /*** Messages **/
    "invalidPaginationValueMessage": "Enter a valid positive number",
    "invalidPageNumberValueMessage": "Enter a valid page number",

    /*** Other *****/
    "customSelectOptionValue": "custom"
};