"use strict";


/* Re-loads the displayed events to match the current state */
var refreshEventsDisplay = (function() {

    var displayedEvents   = document.getElementById("events");
    var eventsCountNumber = document.getElementById("events-count-number");


    /* Create and returns HTML element representing given event object */
    var createEventElement = function(eventObj, id) {
        var eventEl   = document.createElement("li");
        
        eventEl.classList.add("event");
        eventEl.setAttribute("id", id);

        eventEl.appendChild(createIdElement(id));
        eventEl.appendChild(createPropertiesSection(eventObj));
        eventEl.appendChild(createProductsSection(eventObj));

        return eventEl;
    };


    /* Create and return HTML element containing Id of the event */
    var createIdElement = function(id) {
        var idContainerEl = document.createElement("div");
        var idEl          = document.createElement("a");

        idEl.textContent = "#" + id;
        idEl.setAttribute("href", "#" + id);
        idEl.classList.add(CONFIG["anchorClass"]);
        idContainerEl.classList.add(CONFIG["idElementClass"]);

        idContainerEl.appendChild(idEl);

        return idContainerEl;
    };


   /*** Create and return <div> containing basic properties (visitor, date,
        random) of the given event ***/
    var createPropertiesSection = function(eventObj) {
        var propertiesSection = document.createElement("div");
        var date              = decodeTimestamp(eventObj.timestamp);
        var dateValue         = date.toLocaleDateString() + " "
                                + date.toLocaleTimeString();
        var visitorEl         = document.createElement("a");

        visitorEl.textContent = eventObj.visitor_id;
        visitorEl.classList.add(CONFIG["anchorClass"]);
        visitorEl.setAttribute("tabindex", CONFIG["visitorAnchorTabIndex"]);
        visitorEl.setAttribute("title", CONFIG["visitorAnchorTitle"]);
        visitorEl.addEventListener("click", visitorClick(eventObj.visitor_id));

        propertiesSection.classList.add(CONFIG["propertiesSectionClass"]);
        propertiesSection.appendChild(
            createPropertyElement(CONFIG["visitorPropertyName"],
                                  visitorEl,
                                  CONFIG["visitorPropertyClass"])
        );
        propertiesSection.appendChild(
            createPropertyElement(CONFIG["datePropertyName"], dateValue,
                                  CONFIG["datePropertyClass"])
        );
        propertiesSection.appendChild(
            createPropertyElement(CONFIG["randomPropertyName"],
                                  eventObj.random,
                                  CONFIG["randomPropertyClass"])
        );

        return propertiesSection;
    };


    /* Create and return <div> containing all the product lists (viewed
       products, added products, purchased products, car and impressions)
       associated with the given event */
    var createProductsSection = function(eventObj) {

        /* If products is not null appends the created HTML element representing
           given products to productsSection */
        var appendProducts = function(products, label) {
            if (products) {
                productsSection.appendChild(createProductsElement(products,
                                                                  label));
            }
        };

        var productsSection = document.createElement("div");
        
        productsSection.classList.add(CONFIG["productsSectionClass"]);

        /* impressions */
        if (eventObj.impressions) {
            /* Creates empty element without any products */
            var impressionsEl = createProductsElement(
                null, CONFIG["impressionsLabel"]
            );

            impressionsEl.classList.add(
                CONFIG["outerProductsContainerClass"]
            );
            
            for (var i = 0; i < eventObj.impressions.length; i++) {
                var impression = eventObj.impressions[i];
                var productsEl = createProductsElement(
                    impression.item_impressions, impression.feature_id
                );

                impressionsEl.appendChild(productsEl);
            }

            productsSection.appendChild(impressionsEl);
        }

        /* viewed_products, added_products, purchased_products, cart */
        appendProducts(eventObj.cart, CONFIG["cartLabel"])
        appendProducts(eventObj.viewed_products,
                       CONFIG["viewedProductsLabel"]);
        appendProducts(eventObj.added_products,
                       CONFIG["addedProductsLabel"]);
        appendProducts(eventObj.purchased_products,
                       CONFIG["purchasedProductsLabel"]);

        return productsSection;
    };


    /* Create and return a <div> containing 2 <span>s, which contain given
       name (1st) and value (2nd). If given class is not null it adds it to the
       <div>. value can either be String or HTML element to set as a child of
       2nd <span>. */
    var createPropertyElement = function(name, value, className) {
        var propertyEl = document.createElement("div");
        var nameEl     = document.createElement("span");
        var valueEl    = document.createElement("span");

        nameEl.classList.add(CONFIG["propertyNameClass"]);
        valueEl.classList.add(CONFIG["propertyValueClass"]);
        propertyEl.classList.add(CONFIG["propertyClass"]);

        nameEl.textContent  = name;
        if (typeof value === "string") {
            valueEl.textContent = value;
        } else {
            valueEl.appendChild(value);
        }

        propertyEl.appendChild(nameEl);
        propertyEl.appendChild(valueEl);

        if (className) {
            propertyEl.classList.add(className);
        }

        return propertyEl;
    };

    
    /* Create <div> containing list of products followed by given label. 
       If productsObj is null, don't create list and return empty container -
       - needed for handling impressions. */
    var createProductsElement = function(productsObj, label) {
        var containerEl = document.createElement("div");
        var labelEl     = document.createElement("div");
        var images      = [];

        labelEl.textContent = label;
        labelEl.classList.add(CONFIG["productsLabelClass"]);
        containerEl.appendChild(labelEl);

        if (productsObj) {
            var productsListEl = document.createElement("ol");

            for (var j = 0; j < productsObj.length; j++) {
                var productEl = createProductElement(productsObj[j]);
                images.push(productEl.getElementsByTagName("img")[0]);
                productsListEl.appendChild(productEl);
            }

            productsListEl.classList.add(CONFIG["productsListClass"]);
            
            var collapsingProductsEl = createCollapsingPanel(productsListEl,
                                                             images);
   
            collapsingProductsEl.classList.add(
                CONFIG["productsListContainerClass"]
            );
            containerEl.appendChild(collapsingProductsEl);
        }
        
        containerEl.classList.add(CONFIG["productsContainerClass"]);

        return containerEl;
    };


    /* Create and return <li> element representing given product */
    var createProductElement = function(productObj) {
        /* Most products are stored as object lists, but impresssions
           are list of ids - in that case just use object which is id */
        var productId      = productObj.id ? productObj.id : productObj;
        var productEl      = document.createElement("li");
        var anchorEl       = document.createElement("a");
        var productImageEl = document.createElement("img");

        anchorEl.classList.add(CONFIG["anchorClass"]);
        anchorEl.setAttribute("tabindex", CONFIG["productAnchorTabIndex"]);
        anchorEl.setAttribute("title", CONFIG["productAnchorTitle"]);
        anchorEl.addEventListener("click", productClick(productId));
        productEl.classList.add(CONFIG["productClass"]);
        productImageEl.setAttribute("src", getImage(productId));

        /* Mark products that the interact after recommendation */
        if (productObj.f) {
            productEl.classList.add(CONFIG["productRecommendedClass"]);
        }

        anchorEl.appendChild(productImageEl);
        productEl.appendChild(anchorEl);

        addZoomOnHover(productEl);

        return productEl;
    };

    
    /* Create and return collapsing panel containing given element (the elment
       that will collapse and expand). The panel is initially collapsed. Add
       CSS classes, expanding button, also events that will expand it. 
       images is a list of all images elements that this panel has. */
    var createCollapsingPanel = function(collapsingElement, images) {

        var collapsePanel = function() {
            panelContainer.classList.remove(CONFIG["panelCollapsedClass"]);
            panelContainer.classList.add(CONFIG["panelExpandedClass"]);

            expandButton.removeEventListener("click", collapsePanel);
            expandButton.addEventListener("click", expandPanel);
        };

        var expandPanel = function() {
            panelContainer.classList.remove(CONFIG["panelExpandedClass"]);
            panelContainer.classList.add(CONFIG["panelCollapsedClass"]);

            expandButton.removeEventListener("click", expandPanel);
            expandButton.addEventListener("click", collapsePanel);
        };

        var panelContainer  = document.createElement("div");
        var expandButton    = document.createElement("button");

        collapsingElement.classList.add(CONFIG["collapsingPanelElementClass"]);

        expandButton.textContent = CONFIG["collapsingPanelButtonText"];
        expandButton.classList.add(CONFIG["collapsingPanelButtonClass"]);

        panelContainer.classList.add(CONFIG["collapsingPanelClass"]);
        panelContainer.appendChild(collapsingElement);
        panelContainer.appendChild(expandButton);

        expandButton.addEventListener("click", collapsePanel);
        /* Initially hide the button - show it later if needed */
        expandButton.style.display = "none";

        addCollapseNeedCheck(panelContainer, collapsingElement, expandButton,
                            images);

        return panelContainer;
    };


    /* Check if the panel does need to be collapsed - if it has only single
       line then it doesn't and the expand button should be hidden. That
       can be checked only after appending panel to DOM, for which wait using
       MutationObserver. Then check for changes on each of the images load.
       Also add further checks after browser resize. images is list of all
       images element inside the panel. */
    var addCollapseNeedCheck = function(panelContainer, collapsingElement,
                                        expandButton, images) {

        /* First check if that call was made by what we are waiting for - if
           panel is already in DOM. If it is, check if it needs collapsing. */
        var panelInDOM = function(mutationRecords, observerInstance) {
            var box = panelContainer.getBoundingClientRect();

            /* Check if panel already in DOM */
            if (box.left > 0 || box.top > 0) {
                /* Stop observing for changes in DOM for that panel */
                observerInstance.disconnect();

                updateCollapseNeed();
                
                /* Update info on each of the images load */
                for (var i = 0; i < images.length; i++) {
                    var imageEl = images[i];
                    imageEl.addEventListener("load", updateCollapseNeed);
                }

                /* Update info again on each document (browser) resize.
                   Timeouts will take care of not invoking it too often. */
                window.addEventListener("resize", updateCollapseNeed);
            }
        };

        /* Check if given panel actually needs collapsing by collapsing it
           and checking if the size changed. Make sure that it isn't invoked
           too often from browser's resize events by using timeouts. */
        var updateCollapseNeed = (function() {

            var timeoutSet = false;

            var doUpdate = function() {
                panelContainer.classList.remove(CONFIG["panelExpandedClass"]);
                panelContainer.classList.add(CONFIG["panelCollapsedClass"]);

                var panelHeight = panelContainer.getBoundingClientRect().height;
                var elementHeight = collapsingElement.getBoundingClientRect().height;

                if (elementHeight > panelHeight) {
                    expandButton.style.removeProperty("display");
                    panelContainer.classList.remove(
                        CONFIG["collapsingPanelDisabled"]
                    );
                } else {
                    panelContainer.classList.add(
                        CONFIG["collapsingPanelDisabled"]
                    );
                    panelContainer.classList.remove(
                        CONFIG["panelCollapsedClass"]
                    );
                    panelContainer.classList.add(CONFIG["panelExpandedClass"]);
                    expandButton.style.display = "none";
                }

                timeoutSet = false;
            };

            return function() {
                if (!timeoutSet) {
                    setTimeout(doUpdate, CONFIG["collapsingPanelRefreshTime"]);
                    timeoutSet = true;
                }
            };
        }());

        var observer = new MutationObserver(panelInDOM);
        /* Look for when the current event will be added to displayedEvents,
           which is the first moment when it will appear in DOM */
        observer.observe(displayedEvents, { childList: true });

    };

    
    /* Adds events zooming the image when given product element is hovered */
    var addZoomOnHover = (function() {
        var productsGallery = document.getElementById("product-gallery");

        return function(productEl) {

            var imageMouseEnter = function(event) {
                /* To guarantee that the image renders fully move it to other
                   container, as staying in products list might cut the image
                   because of collapsing panel's "overflow: hidden" */

                var productBox  = productEl.getBoundingClientRect();
                var galleryBox  = productsGallery.getBoundingClientRect();
                var imageOffset = image.naturalHeight - productBox.height;

                /* Ensure that the product won't collapse when image removed
                   from it */
                productEl.style.width  = productBox.width + "px";
                productEl.style.height = productBox.height + "px";

                /* Calculate anchor coordinates in new position, so that it
                   stays in the same place on the screen */
                anchor.style.left = productBox.left - galleryBox.left + "px";
                anchor.style.top  = productBox.top - galleryBox.top
                                    - imageOffset + "px";
                anchor.classList.add(CONFIG["productImageZoomedClass"]);

                /* Remove mouseenter event listener to prevent Firefox (tested
                   on Firefox 34) from firing mouseenter like crazy after
                   appending it into other place in DOM */
                image.removeEventListener("mouseenter", imageMouseEnter);

                productsGallery.appendChild(anchor);
            };

            /* Remove changes made by mouseenter */
            var imageMouseLeave = function(event) {
                anchor.classList.remove(CONFIG["productImageZoomedClass"]);
                anchor.style.removeProperty("left");
                anchor.style.removeProperty("top");
                productEl.style.removeProperty("height");
                productEl.style.removeProperty("width");

                /* Move it back to original place */
                anchorParent.appendChild(anchor);

                /* Add removed listener on mouseenter */
                image.addEventListener("mouseenter", imageMouseEnter);
            };

            var anchor       = productEl.getElementsByTagName("a")[0];
            var image        = productEl.getElementsByTagName("img")[0];
            var anchorParent = anchor.parentElement;

            /* Wait with adding zoom until the image fully loads */
            image.addEventListener("load", function() {
                /* When mouse enters the product, make the image natural height
                   (not collapsing the container) and move it a bit up, so that
                   it stays centered */
                image.addEventListener("mouseenter", imageMouseEnter);
                image.addEventListener("mouseleave", imageMouseLeave);
            });
        };
    }());


    /* Returns a Date object set to a date represented by a given timestamp */
    var decodeTimestamp = (function() {
        /* Regular expressions that extracts 6 numbers from the timestamp */
        var timestampDecoder = /^(\d{4})(\d{2})(\d{2})T(\d{2})(\d{2})(\d{2})Z$/;

        return function(timestamp) {
            /* Extract date information from timestamp */
            var dateTimeDetails = timestampDecoder.exec(timestamp);

            return new Date(dateTimeDetails[1], dateTimeDetails[2],
                            dateTimeDetails[3], dateTimeDetails[4],
                            dateTimeDetails[5], dateTimeDetails[6]);
        };
    }());


    /* For given visitor id return handler for clicking that visitor element.
       Handler filteres the events by that visitor id. */
    var visitorClick = function(visitorId) {
        return function(event) {
            filterEventsByVisitor(visitorId);
            event.preventDefault();
            saveAppState();
            refreshPagesList();
            refreshEventsDisplay();
        };
    };


    /* For given product id return handler for clicking that product element.
       Handler filteres the events by that product. */
    var productClick = function(productId) {
        return function(event) {
            filterEventsByProduct(productId);
            event.preventDefault();
            saveAppState();
            refreshPagesList();
            refreshEventsDisplay();
        };
    };


    return function() {
        var firstEventOnPage = state.eventsPerPage * (state.currentPage - 1);
        var lastEventOnPage  = firstEventOnPage + state.eventsPerPage - 1;

        eventsCountNumber.textContent = String(events.length);
        
        if (lastEventOnPage >= events.length) {
            lastEventOnPage = events.length - 1;
        }

        /* Removes all previously displayed events */
        removeAllChildren(displayedEvents);

        /* Add event to the list one by one */
        for (var i = firstEventOnPage; i <= lastEventOnPage; i++) {
            displayedEvents.appendChild(createEventElement(events[i], i + 1));
        }
        
        window.scroll(0, 0);
    };

}());