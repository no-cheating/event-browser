"use strict";


/* Namespace for variables (to use only in this module) that need to be shared
   between multiple global functions */
var _filterEnv = {
    /*** Properties ***/
    "showAllButton": undefined,

    /*** Functions ***/
    /* Defined lower in the file */
    "filterEvents"           : undefined,
    "visitorPredicate"       : undefined,
    "productPredicate"       : undefined,
    "hideShowAllEventsButton": undefined,
    "showShowAllEventsButton": undefined
}


/* Initializes this module */
var initializeFiltering = (function() {

    var showAllEventsClick = function(event) {
        disableAllFilters();
        saveAppState();
        refreshPagesList();
        refreshEventsDisplay();
    };

    return function() {
        _filterEnv.showAllButton = document.getElementById("show-all-events");

        _filterEnv.showAllButton.addEventListener("click", showAllEventsClick);

        if (!state.filterName) {
            _filterEnv.hideShowAllEventsButton();
        }
    };

}())


/* Show only events with given visitor id */
var filterEventsByVisitor = (function() {

    /* For given visitor id return predicate that returns true only for events
       with that visitor id */
    var visitorPredicate = function(visitorId) {
        return function(eventObj) {
            return (eventObj.visitor_id === visitorId);
        };
    };


    return function(visitorId) {
        state.filterValue    = visitorId;
        _filterEnv.filterEvents(visitorPredicate(visitorId),
                                filterEventsByVisitor._filterName);
    };

}());

filterEventsByVisitor._filterName = CONFIG["filterVisitorName"];


/* Show only events containing anywhere product with given id */
var filterEventsByProduct = (function() {

    /* For given product id return predicate that returns true only for events
       containing any info about product with that id in any of it's
       properties */
    var productPredicate = function(productId) {
        return function (eventObj) {
            var productFound = false;
            var listsToCheck = [ "viewed_products", "added_products",
                                "purchased_products", "cart" ];

            for (var i = 0; i < listsToCheck.length; i++) {
                var listName = listsToCheck[i];

                if (eventObj[listName]) {
                    if (productOnList(productId, eventObj[listName])) {
                        productFound = true;
                        break;
                    }

                }
            }

            /* impressions has to be processed separately, as it has another
               schema */
            if (!productFound && eventObj.impressions) {
                for (var i = 0; i < eventObj.impressions.length; i++) {
                    var idList = eventObj.impressions[i].item_impressions;
                    for (var j = 0; j < idList.length; j++) {
                        if (idList[j] === productId) {
                            productFound = true;
                            break;
                        }
                    }
                }
            }
            
            return productFound;
        };
    };
    

    /* Return if product with given id is on the given list of products */
    var productOnList = function(productId, list) {
        for (var i = 0; i < list.length; i++) {
            if (list[i].id === productId) {
                return true;
            }
        }

        return false;
    };


    return function(productId) {
        state.filterValue    = productId;
        _filterEnv.filterEvents(productPredicate(productId),
                                filterEventsByProduct._filterName);
    };

}());

filterEventsByProduct._filterName = CONFIG["filterProductName"];


/* Cancels any filters applied and restore all events into view */
var disableAllFilters = function() {
    state.filterName     = undefined;
    state.filterValue    = undefined;
    events               = eventsAll;

    _filterEnv.hideShowAllEventsButton();
    changePagination(1, null);
};


/* Return filtering function that was stored in the state URL by the given
   name */
var getFilterFunction = (function() {
    var nameToFunctionDict = {};
    var filtersList        = [ filterEventsByVisitor, filterEventsByProduct ];

    for (var i = 0; i < filtersList.length; i++) {
        var filterFunction = filtersList[i];
        nameToFunctionDict[filterFunction._filterName] = filterFunction;
    }

    return function(name) {
        return nameToFunctionDict[name];
    };
}());


/* Show only events that pass given predicate. Predicate is a function that
   is given 1 argument, event object, and returns boolean. */
_filterEnv.filterEvents = function(predicate, filterName) {
    state.filterName = filterName;
    events = eventsAll.filter(predicate);
    /* Go to the first page of results */
    changePagination(1, null);

    /* It can be invoked before the module is initialized */
    if (_filterEnv.showAllButton) {
        _filterEnv.showShowAllEventsButton();
    }
};


/* Dual function that hide and show button that disables all filters and
   loads all events into application */
_filterEnv.hideShowAllEventsButton = function() {
    _filterEnv.showAllButton.style.display = "none";
};

_filterEnv.showShowAllEventsButton = function() {
    _filterEnv.showAllButton.style.removeProperty("display");
};