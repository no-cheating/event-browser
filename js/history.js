/* Current state of the application */
var state = {
    "eventsPerPage" : undefined,   /* How many events are displayed on 1 page */
    "currentPage"   : undefined,   /* Number of current page (indexed from 1) */
    "filterName"    : undefined,   /* Which filtered is applied */
    "filterValue"   : undefined    /* What is the value for filtered property */
};


var initializeHistoryManagement = function() {
    window.addEventListener("popstate", function(event) {
        /* Some browsers fire popstate event on newly loaded pages. It can
           happen before we run our app and save any state and event.state would
           be null then. If it happens after we set our state, the state should
           be the same as current one and no changes will be applied. */
        if (event.state) {
            setAppState(event.state, true);
        }
    });
};


/* Save the application state in the browser history and in the URL */
var saveAppState = (function() {

    /* Returns a String representation of given state for use as a URL. */
    var stateToURL = function(stateObj) {
        var URL     = CONFIG["stateStartMarker"];
        
        for (var property in stateObj) {
            if (stateObj.hasOwnProperty(property)
                && stateObj[property] !== undefined) {

                /* Skip ampersand on first property */
                if (URL !== CONFIG["stateStartMarker"]) {
                    URL += CONFIG["statePropertySeparator"];
                }

                URL += saveAppState._propertyDict[property] + "="
                       + stateObj[property];
            }
        }

        return URL;
    };

    return function() {
        history.pushState(state, "", stateToURL(state));
    };
}());


/* Translates the names of the state properties to names of URL arguments */
saveAppState._propertyDict = {
    "eventsPerPage" : "perPage",
    "currentPage"   : "pageNo",
    "filterName"    : "filterN",
    "filterValue"   : "filterV"
};



/* Restore the application state from current URL. Return true on succeess or
   false on fail (when URL is invalid). Still the function only checks
   properties for type validity - values can still be invalid. That deeper part
   of value validation is left to initialization functions of the modules that
   use them.

   The URL doesn't have to contain every property. Those of properties that
   are included are set, for others initial values will be found. Still if any
   included property has invalid value, the whole state is invalidated and
   any property value isn't used. Any unexpected arguments in URL are ignored */
var restoreAppState = (function() {

    var separators = new RegExp("=|" + CONFIG["statePropertySeparator"]);

    /* Perform conversion from URL to state object. Return state on success or
       null when unable to create a valid state from given URL. */
    var stateFromURL = function(URL) {
        var stateObj         = {}
        var stateDescription = URL.split(CONFIG["stateStartMarker"]);
        stateDescription     = stateDescription[stateDescription.length - 1];

        /* Splits state string into list of subsequent key and values */
        var properties = stateDescription.split(separators);

        /* Assign value to every property. The values are still strings. */
        for (var i = 0; i < properties.length; i += 2) {
            var propertyName  = restoreAppState._propertyDict[properties[i]];
            var propertyValue = properties[i + 1];

            stateObj[propertyName] = propertyValue;
        }

        /* Convert strings to numbers where needed */
        if (stateObj.eventsPerPage) {
            stateObj.eventsPerPage = parseInt(stateObj.eventsPerPage);
        }
        if (stateObj.currentPage) {
            stateObj.currentPage = parseInt(stateObj.currentPage);
        }

        /* Basic type validation of properties:
           - check eventsPerPage and currentPage against being NaN
           - make sure both filterName and filterValue are defined or none */
        if ((stateObj.eventsPerPage !== stateObj.eventsPerPage)
            || (stateObj.currentPage !== stateObj.currentPage)
            || (stateObj.filterName && !stateObj.filterValue)
            || (!stateObj.filterName && stateObj.filterValue)) {

            return null;
        } else {
            return stateObj;
        }
    };


    return function() {
        var savedState = stateFromURL(window.location.search);

        if (savedState) {
            setAppState(savedState, false);
            
            return true;
        } else {
            return false;
        }
    };
}());

/* Stores reverse translation than saveAppState._propertyDict */
restoreAppState._propertyDict = (function() {
    var dict = {};

    for (var property in saveAppState._propertyDict) {
        if (saveAppState._propertyDict.hasOwnProperty(property)) {
            var value   = saveAppState._propertyDict[property];
            dict[value] = property;
        }
    }

    return dict;
}());


/* Set application state to the given one and refresh display */
var setAppState = (function() {

    /* Given old and new state objects and name of state property, return
       the value of that property in the new state, when it's different from the
       value in the old state (property has changed it's value). Return null
       when they're equal (the property hasn't changed). */
    var propertyOnlyWhenChanged = function(oldState, newState, propertyName) {
        if (oldState[propertyName] !== newState[propertyName]) {
            return newState[propertyName];
        } else {
            return null;
        }
    };


    return function(newState, refreshDisplay) {
        var anyChange         = false;
        var newEventsPerPage  = propertyOnlyWhenChanged(state, newState,
                                                       "eventsPerPage");
        var newCurrentPage    = propertyOnlyWhenChanged(state, newState,
                                                       "currentPage");
        var newFilterProperty = propertyOnlyWhenChanged(state, newState,
                                                        "filterName");
        var newFilterValue    = propertyOnlyWhenChanged(state, newState,
                                                        "filterValue");

        /* Need to check newFilterProperty exactly against null, beacause it's
           new value can be undefined */
        if (newFilterProperty !== null || newFilterValue) {
            anyChange = true;

            /* Check if we turn off filter (newFilterProperty is undefined) or
               turn it on */
            if (newFilterProperty === undefined) {
                disableAllFilters();
            } else {
                /* Have to use filterName value from the state, because
                   newFilterProperty might be null */
                (getFilterFunction(newState.filterName))(newFilterValue);
            }
        }
        if (newEventsPerPage || newCurrentPage) {
            anyChange = true;
            changePagination(newCurrentPage, newEventsPerPage);
        }

        if (anyChange && refreshDisplay) {
            refreshPagesList();
            refreshEventsDisplay();
        }
    };

}());