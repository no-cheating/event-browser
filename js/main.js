"use strict";


/* All events in the current state (i.e. after applying any filters) */
var events = eventsAll;


(function() {

    /* Main function of the application */
    var runApp = function() {
        var URLHash = window.location.hash;

        /* TODO: Change for sorting by date */
        sortEvents(eventsAll, sortEvents.compareEventsByDate);

        /* Try loading the state from URL */
        restoreAppState();

        initializeHistoryManagement();
        initializeFiltering();
        initializePagination();

        /* Save initial state */
        saveAppState();

        /* Displays the first batch of events */
        refreshEventsDisplay();

        /* Use hash value if one was provided in URL. Needed to be re-entered
           because saveAppState erases hash by changing URL on pushState */
        if (URLHash) {
            window.location.hash = URLHash;
        }
    };


    document.addEventListener("DOMContentLoaded", runApp);

}());