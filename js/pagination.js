"use strict";


/* Namespace for variables (to use only in this module) that need to be shared
   between multiple global functions */
var _paginationEnv = {
    /* Properties */
    "pagesLists": undefined,
    
    /* Functions (defined lower in the file) */
    "pagesCount" : undefined,
    "setSelect"  : undefined,
    "showNumberInputMessage": undefined,
    "hideNumberInputMessage": undefined,
    "customPageNumbers"     : undefined
};


/* Change page number or number of events displayed per page. Both values can
   be given at once or one of them can be omitted by setting to null. */
var changePagination = (function() {
    var settingsMessage = document.getElementById("custom-pagination-message");
    var pageNumMessages = document.getElementsByClassName("custom-page-number-message");
    
    return function(newPage, newEventsPerPage) {
        /* Change events per page */
        if (newEventsPerPage) {
            /* Remove any previously displayed message */
            settingsMessage.textContent = "";

            if (state.eventsPerPage !== newEventsPerPage) {
                /* Calculate current number of page in the new pagination
                   setting, so that (at least) first event from the previous
                   screen is still displayed */
                var firstDisplayedEvent  = state.eventsPerPage
                    * (state.currentPage - 1);
                state.currentPage        = Math.ceil((firstDisplayedEvent + 1) /
                                                     newEventsPerPage);
                state.eventsPerPage      = newEventsPerPage;

                _paginationEnv.setSelect(newEventsPerPage);
            }
        }

        /* Change page number */
        if (newPage) {
            /* Remove any previously displayed message */
            doForEachOnList(pageNumMessages, function(message) {
                _paginationEnv.hideNumberInputMessage(message);
            });

            if (newPage !== state.currentPage) {
                state.currentPage = newPage;
            }
        }
    };
}());


/* Initializes pagination components and variables */
var initializePagination = (function() {

    var pageNumberForms    = document.getElementsByClassName("custom-page-number");
    var pageNumberInputs   = [];  /* Set later in clonePageNumberList */
    var pageNumberButtons  = [];
    var pageNumberMessages = document.getElementsByClassName("custom-page-number-message");

    var paginationForm            = document.getElementById("pagination-settings");
    var paginationSelect          = paginationForm.elements["basic-pagination"];
    var paginationCustomPanel     = paginationForm.elements["custom-pagination"]
    var paginationCustomInput     = paginationForm.elements["custom-value"];
    var paginationCustomButton    = paginationForm.elements["custom-submit"];
    var paginationCustomMessage   = document.getElementById("custom-pagination-message");
    var paginationCustomDisplayed = true;

    _paginationEnv.pagesLists = document.getElementsByClassName("page-numbers");


    /* Find initial values for pagination settings.It might happen that the
       application was loaded from a URL containing a saved state. In that
       case the pagination settings might be already set but not yet fully
       validated. Validate the state values. If they are invalid, initialize
       them correctly. */
    var initializeSettings = function() {
        /* eventsPerPage validation*/
        if (typeof state.eventsPerPage !== "number"
            || state.eventsPerPage !== state.eventsPerPage
            || state.eventsPerPage <= 0) {

            state.eventsPerPage = findInitialEventsPerPage();
        } else {
            /* eventsPerPage was loaded from URL. Set the select control to
               reflect the current eventsPerPageValue */
            _paginationEnv.setSelect(state.eventsPerPage);
        }

        /* pageNumber validation */
        if (typeof state.currentPage !== "number"
            || state.currentPage !== state.currentPage
            || state.currentPage <= 0
            || state.currentPage > _paginationEnv.pagesCount()) {

            state.currentPage = CONFIG["initialPageNumber"];
            console.log(state.currentPage);
        }
    };


    /* Find and return initial value for eventsPerPage. Use the default option
       of the select. If any isn't marked as default pick the middle one. */
    var findInitialEventsPerPage = function() {
        var selectValue = paginationSelect.value;
        var options     = paginationSelect.children;
        /* Selecting the middle of all the options should exclude "custom", so
           we subtract 2 from length instead of 1 */
        var chosenIndex = Math.floor((options.length - 2) / 2);

        for (var i = 0; i < options.length; i++) {
            if (options[i].getAttribute("selected") !== null) {
                chosenIndex = i;
                break;
            }
        }

        paginationSelect.selectedIndex = chosenIndex;
        hideCustomPanel();

        return parseInt(options[chosenIndex].value);
    };
    
    
    /* Clones bottom pagination panel and set it as top pagination panel */
    var clonePageNumberList = function() {
        var bottomPanel = document.getElementById("pagination-bottom");
        
        bottomPanel.appendChild(
            document.getElementById("page-selection").cloneNode(true)
        );
        
        /* Those inputs will be removed from DOM while creating new list and so
           they would disappear from HTMLCollection after that. To avoid missing
           their references, the collection is transformed into regular array */        
        _paginationEnv.customPageNumbers = [];
        for (var i = 0; i < pageNumberForms.length; i++) {
            _paginationEnv.customPageNumbers[i] = pageNumberForms[i];
            pageNumberInputs[i]  = pageNumberForms[i].elements["custom-value"];
            pageNumberButtons[i] = pageNumberForms[i].elements["custom-submit"];
        }
    };

    
    /* Adds JS events invoked when the user changes the pagination setting */
    var initializeControlsEvents = function() {

        var selectChange = function() {
            var eventsPerPageNew = paginationSelect.value;

            if (eventsPerPageNew === CONFIG["customSelectOptionValue"]
                && !paginationCustomDisplayed) {

                showCustomPanel();
            } else {
                if (paginationCustomDisplayed) {
                    hideCustomPanel();
                }
                changePagination(null, parseInt(eventsPerPageNew));
                saveAppState();
                refreshPagesList();
                refreshEventsDisplay();
            }
        };

        var customPaginationSubmit = function() { 
            var eventsPerPageNew = parseInt(paginationCustomInput.value);

            /* Check if the entered string is a proper number */
            if (!eventsPerPageNew || eventsPerPageNew <= 0) {
                _paginationEnv.showNumberInputMessage(paginationCustomMessage,
                                       CONFIG["invalidPaginationValueMessage"]);
            } else {
                changePagination(null, eventsPerPageNew);
                saveAppState();
                refreshPagesList();
                refreshEventsDisplay();
            }
        };

        var customPageNumberSubmit = function(inputEl, message) {
            return function() {
                var newPageNumber = parseInt(inputEl.value);

                if (!newPageNumber || newPageNumber < 1
                    || newPageNumber > _paginationEnv.pagesCount()) {

                    _paginationEnv.showNumberInputMessage(message,
                                       CONFIG["invalidPageNumberValueMessage"]);
                } else {
                    changePagination(newPageNumber, null, true, true);
                    saveAppState();
                    refreshPagesList();
                    refreshEventsDisplay();
                }
            };
        };

        /* Returns event handler function, which will run given handler
           function if Enter key was pressed. Also prevents form submission. */
        var enterPress = function(handler) {
            return function(event) {
                if (event.keyCode === 13) {
                    handler();
                    event.preventDefault();     /* To prevent form submission */
                }
            }
        };

        paginationSelect.addEventListener("change", selectChange);
        paginationCustomButton.addEventListener("click",
                                                customPaginationSubmit);
        for (var i = 0; i < pageNumberButtons.length; i++) {                                        
            pageNumberButtons[i].addEventListener("click",
                customPageNumberSubmit(pageNumberInputs[i],
                                       pageNumberMessages[i])
            );
        }

        /* Also allows actions by presssing Enter key */
        paginationCustomInput.addEventListener("keypress",
                                            enterPress(customPaginationSubmit));
        for (var i = 0; i < _paginationEnv.customPageNumbers.length; i++) {
            _paginationEnv.customPageNumbers[i].addEventListener("keypress",
                enterPress(customPageNumberSubmit(pageNumberInputs[i],
                                                  pageNumberMessages[i]))
            );
        }
    };


    /* Shows controls for entering custom pagination settings */
    var showCustomPanel = function() {
        paginationCustomInput.value = undefined;
        paginationCustomPanel.style.removeProperty("display");
        paginationCustomDisplayed = true;
    };


    /* Hide controls for entering custom pagination settings */
    var hideCustomPanel = function() {
        paginationCustomPanel.style.display = "none";
        paginationCustomDisplayed = false;
    };


    /* Set select for events per page to show given value.
       The function is defined here to have access to variables it needs. */
    _paginationEnv.setSelect = function(value) {
        var optionFound = false;
        var options     = paginationSelect.children;
        var customIndex;
        
        /* Try default values */
        for (var i = 0; i < options.length; i++) {
            if (parseInt(options[i].value) === value) {
                paginationSelect.selectedIndex = i;
                hideCustomPanel();
                optionFound = true;
                break;
            }
            else if (options[i].value === CONFIG["customSelectOptionValue"]){
                /* Save index of custom value, as it might be needed later */
                customIndex = i;
            }
        }

        if (!optionFound) {
            /* If any default value doesn't match, use custom input */
            paginationSelect.selectedIndex = customIndex;
            paginationCustomInput.value    = String(value);
        }
    };

    
    return function() {
        initializeSettings();
        clonePageNumberList();                        
        _paginationEnv.hideNumberInputMessage(paginationCustomMessage);
        for (var i = 0; i < pageNumberMessages.length; i++) {
            _paginationEnv.hideNumberInputMessage(pageNumberMessages[i]);
        }
        refreshPagesList();
        initializeControlsEvents();
    };

}());


/* Loads and displays the list of page numbers. The list includes the current
   page, some of the pages nearest to it, also the last and first one. */
var refreshPagesList = (function() {        

    /* Name of HTML element used to store page numbers */
    var PAGE_NUMBER_EL = "a";


    /* Creates and returns <li> containing page number of given number. If
       customNumber element is given, it means that this is the current page and
       the number inside <li> is <input> instead of <a>. */ 
    var createNumberElement = function(pageNo, customNumber) {
        var listElement = document.createElement("li");

        listElement.classList.add(CONFIG["pageNumberClass"]);
        if (customNumber) {
            listElement.classList.add(CONFIG["currentPageNumberClass"]);
            customNumber.elements["custom-value"].value = String(pageNo);
            listElement.appendChild(customNumber);
        } else {
            var pageNumberEl = document.createElement(PAGE_NUMBER_EL);

            pageNumberEl.classList.add(CONFIG["anchorClass"]);
            pageNumberEl.textContent = String(pageNo);
            listElement.appendChild(pageNumberEl);
        }

        return listElement;
    };


    /* Creates and adds first/last page number before given node in the
       given pages list. Also adds given CSS class to the <li> element, if class
       not null. */
    var addOutermostNumber = function(pagesList, number, cssClass, beforeNode) {
        var numberElement = createNumberElement(number, false);

        if (cssClass) {
            numberElement.classList.add(cssClass);
        }
        
        pagesList.insertBefore(numberElement, beforeNode);
    };


    /* Invoked when the user clicks inside pages list */
    var pagesListClicked = function(event) {
        var targetParent = event.target.parentNode;

        /* If the click was on a link to another page, change the page */
        if (targetParent.classList.contains(CONFIG["pageNumberClass"])
            && event.target.nodeName === PAGE_NUMBER_EL.toUpperCase()) {

            changePagination(parseInt(event.target.textContent), null);
            saveAppState();
            refreshPagesList();
            refreshEventsDisplay();
        }
    };


    return function() {
        var pagesCount = _paginationEnv.pagesCount();

        /* Create new list */
        var numbersAmount     = CONFIG["pageNumbersDisplayed"];
        var firstHalfAmount   = Math.floor((numbersAmount - 1) / 2);
        var secondHalfAmount  = numbersAmount - 1 - firstHalfAmount;
        var firstNumber       = state.currentPage - firstHalfAmount;
        var lastNumber        = state.currentPage + secondHalfAmount;      

        /* Make sure that we don't go beyond first and last page */
        firstNumber = (firstNumber >= 1) ? firstNumber : 1;
        lastNumber  = (lastNumber  <= pagesCount) ? lastNumber : pagesCount;

        /* Remove previous list */
        doForEachOnList(_paginationEnv.pagesLists, function(pagesList) {
            removeAllChildren(pagesList);
        });
        

        /* Creating the list */
        for (var pageNumber = firstNumber; pageNumber <= lastNumber;
             pageNumber++) {
                 
            if (pageNumber === state.currentPage) {
                for (var i = 0; i < _paginationEnv.pagesLists.length; i++) {
                    _paginationEnv.pagesLists[i].appendChild(
                        createNumberElement(pageNumber,
                                            _paginationEnv.customPageNumbers[i])
                    );
                }
            } else {
                var numberElement = createNumberElement(pageNumber);
                
                doForEachOnList(_paginationEnv.pagesLists, function(pagesList) {
                    pagesList.appendChild(numberElement.cloneNode(true));
                });
            }
            
        }

        /* Add also the first page (page 1) if not already in the list */
        if (1 < firstNumber) {
            var firstClass = (2 < firstNumber)
                ? CONFIG["firstPageNumberClass"] : null;
            doForEachOnList(_paginationEnv.pagesLists, function(pagesList) {
                addOutermostNumber(pagesList, 1, firstClass,
                                   pagesList.firstElementChild);
            });
        }

        /* Add the last page if not already in the list */
        if (lastNumber < pagesCount) {
            var lastClass = (lastNumber < pagesCount - 1)
                ? CONFIG["lastPageNumberClass"] : null;
            doForEachOnList(_paginationEnv.pagesLists, function(pagesList) {
                addOutermostNumber(pagesList, pagesCount, lastClass, null);
            });
        }

        /* Add click handler on the list */
        doForEachOnList(_paginationEnv.pagesLists, function(pagesList) {
            pagesList.addEventListener("click", pagesListClicked);
        });
    };

}());


/* Returns the number of all pages of the events list */
_paginationEnv.pagesCount = function() {
    return Math.ceil(events.length / state.eventsPerPage);
};


/* Shows given message on number input message elmenent */
_paginationEnv.showNumberInputMessage = function(element, message) {

    var hideThisMessage = function(event) {
        _paginationEnv.hideNumberInputMessage(element);
        element.removeEventListener("click", hideThisMessage);
    };

    element.textContent = message;
    element.style.removeProperty("display");

    element.addEventListener("click", hideThisMessage);
    setTimeout(hideThisMessage, CONFIG["numberInputMessageTime"]);
};


/* Hides given number input messge element */
_paginationEnv.hideNumberInputMessage = function(element) {
    element.style.display = "none";
};