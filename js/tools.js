"use strict";


/* jshint unused: false */
var getImage = function(id) {
  if ('scarab/homepage' === id) {
    return 'images/homepage.png';
  }
  var sub = id.substring(0, id.length-3);
  return 'http://89130063.r.cdn77.net/data/tovar/_m/' + sub + '/m' + id + '.jpg';
};


var removeAllChildren = function(element) {
    while (element.hasChildNodes()) {
        element.removeChild(element.lastChild);
    }
};


/* Invoke given function for each member of the list. action function is given
   the page list as it's sole argument. */
var doForEachOnList = function(list, action) {
    for (var i = 0; i < list.length; i++) {
        action(list[i]);
    }
};


var sortEvents = function(eventsList, comparisonFunc) {    
    eventsList.sort(comparisonFunc);
};


sortEvents.compareEventsByDate = function(event1, event2) {
    if      (event1 < event2) { return -1; }
    else if (event1 > event2) { return 1; }
    else                      { return 0; }
};


sortEvents.compareEventsByProductsAmount = function(event1, event2) {

    var countProducts = function(event) {
        var count = 0;

        if (event.added_products) {
            count += event.added_products.length;
        }

        if (event.viewed_products) {
            count += event.viewed_products.length;
        }

        if (event.purchased_products) {
            count += event.purchased_products.length;
        }

        if (event.cart) {
            count += event.cart.length;
        }

        if (event.impressions) {
            for (var i = 0; i < event.impressions[i]; i++) {
                count += event.impresssions[i].length;
            }
        }

        return count;
    }

    var event1Products = countProducts(event1);
    var event2Products = countProducts(event2);

    if      (event1Products > event2Products) { return -1; }
    else if (event1Products < event2Products) { return 1; }
    else                                      { return 0; }

};